import { LitElement, html, css } from 'lit';
import { property, customElement, state } from 'lit/decorators.js';
import { until } from 'lit/directives/until.js';
import { ChallengeDataService } from '../ChallengeDataService.js';

@customElement('front-end-dev-challenge')
export class FrontEndDevChallenge extends LitElement {
  static styles = css`
    :host {
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      color: #1a2b42;
      max-width: 960px;
      margin: 0 auto;
      text-align: center;
    }

    main {
      flex-grow: 1;
    }

    footer {
      margin: 1rem;
    }

    challenge-chart {
      display: block;
    }
  `;

  constructor() {
    super();

    this.client = new ChallengeDataService();
  }

  async loadData() {
    this._data = await this.client.getDataSet(this._datasetSize);
  }

  @property() name?: string = 'The Data';

  @state()
  private _datasetSize: string = 'small';

  private _data?: ChallengeDataSet;

  client: ChallengeDataService;

  private _handleDatasetSizeChange(e: InputEvent) {
    const { value } = e.target as HTMLSelectElement;
    this._datasetSize = value;
  }

  private renderChart() {
    if (!this._data) {
      return null;
    }

    const headers = [this._data.xColumn.name, this._data.yColumn.name];
    const { values: xValues } = this._data.xColumn;
    const { values: yValues } = this._data.yColumn;
    const points = xValues.map((x: number, i: number) => ({
      x,
      y: yValues[i],
    }));

    return html`
      <challenge-chart .data=${points}></challenge-chart>
      <challenge-table .headers=${headers} .points=${points}></challenge-table>
    `;
  }

  protected render() {
    return html`
      <header>
        <h1>${this.name}</h1>
      </header>
      <main>
        <label for="dataset-size">Dataset Size</label>
        <select id="dataset-size" @input=${this._handleDatasetSizeChange}>
          <option value="small">Small</option>
          <option value="medium">Medium</option>
          <option value="large">Large</option>
        </select>
        ${until(
          this.loadData().then(() => this.renderChart()),
          html`<div>Loading...</div>`
        )}
      </main>
      <footer>
        Authored by <a href="https://mossymaker.com/" rel="me">Patrick Walsh</a>
      </footer>
    `;
  }
}
