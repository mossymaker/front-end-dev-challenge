import { LitElement, html, css } from 'lit';
import { property, customElement } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';

@customElement('challenge-table')
export class ChallengeTable extends LitElement {
  static styles = css`
    :host {
      --border-dark: #888;
      --border-light: #dedede;
    }
    table {
      border: 1px solid var(--border-dark);
      border-collapse: collapse;
      margin: 1rem 0;
    }

    thead {
      background-color: #333;
    }
    thead th {
      color: #bfbfbf;
    }

    tbody td {
      border-bottom: 1px solid var(--border-light);
      border-right: 1px solid var(--border-light);
      text-align: right;
      font-family: monospace;
    }
    tbody tr:last-of-type td {
      border-bottom: 1px solid var(--border-dark);
    }

    th,
    td {
      padding: 0.25rem 0.5rem;
      white-space: nowrap;
      width: 50%;
    }

    th:last-child,
    td:last-child {
      border-right: 0;
    }
  `;

  @property() headers?: Array<string>;

  @property() points?: Array<{ x: Number; y: Number }>;

  renderHead() {
    if (!this.headers) {
      return null;
    }
    return html`
      <thead>
        <tr>
          ${this.headers.map(header => html`<th>${header}</th>`)}
        </tr>
      </thead>
    `;
  }

  render() {
    return html`
      <table>
        ${this.renderHead()}
        <tbody>
          ${this.points &&
          repeat(
            this.points,
            point => html`
              <tr>
                <td>${point.x}</td>
                <td>${point.y}</td>
              </tr>
            `
          )}
        </tbody>
      </table>
    `;
  }
}
