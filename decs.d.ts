interface ChallengeDataColumn {
  name: string;
  values: Array<number>;
}
interface ChallengeDataSet {
  name: string;
  xColumn: ChallengeDataColumn;
  yColumn: ChallengeDataColumn;
}
