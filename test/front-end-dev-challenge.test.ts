import { html, fixture, expect } from '@open-wc/testing';
import { FrontEndDevChallenge } from '../src/FrontEndDevChallenge.js';

describe('FrontEndDevChallenge', () => {
  let element: FrontEndDevChallenge;
  beforeEach(async () => {
    element = await fixture(
      html`<front-end-dev-challenge></front-end-dev-challenge>`
    );
  });

  it('renders an h1', () => {
    const h1 = element.shadowRoot!.querySelector('h1')!;
    expect(h1).to.exist;
    expect(h1.textContent).to.equal('The Data');
  });

  it('passes the a11y audit', async () => {
    await expect(element).shadowDom.to.be.accessible();
  });
});
