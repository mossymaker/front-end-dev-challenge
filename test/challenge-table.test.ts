import { html, fixture, expect } from '@open-wc/testing';

import { ChallengeTable } from '../src/ChallengeTable.js';

describe('ChallengeTable', () => {
  let element: ChallengeTable;
  beforeEach(async () => {
    element = await fixture(
      html`<challenge-table
        .headers=${['x', 'y']}
        .points=${[{ x: 0, y: 0 }]}
      ></challenge-table>`
    );
  });

  it('renders a table', () => {
    const table = element.shadowRoot!.querySelector('table')!;
    expect(table).to.exist;
  });

  it('passes the a11y audit', async () => {
    await expect(element).shadowDom.to.be.accessible();
  });
});
